ABOUT
-----
  This module implements a mapping functionality between Salesforce custom field
  and Drupal office hours field.

REQUIREMENTS
------------
  You need to have salesforce module enabled and configurated.
  You need to have Office hours module enabled and created some fields.
  Apply this patch https://www.drupal.org/node/2669630
  Enabled salesforce_push, salesforce_pull or both.
  You have to create three custom field in your Salesforce structure.
  In order to create the three different fields in Salesforce, the following
  steps should be followed:
    - Go to Setup > Search 'Account' > Fields
    - You should see the first block containing all the standard fields and a
    second block below this one containing the custom fields
    - Scroll down to the custom fields block and click on the 'New' button
    - The format of the field should be a text (multi-picklist may have been
    chosen as well, but in order to have some freedom, Text was chosen)
    - Select the Text format and click next
    - Enter the field label (e.g Opening Days, Opening Hours and Closing Hours),
    the length (255 is enough) and the API
    Name (eg. OpeningDays, OpeningHours, ClosingHours)
    - Description and Help text are not mandatory
    - The field is not mandatory nor unique nor an external Id and no default
    value should be used
    - Click on 'Next'
    - Leave the default profile permissions
    - Click on 'Next'
    - Remove all layout
    - Click on 'Save'
    - Place the field on the layout if needed

HOW IT WORKS
------------
  We send data in Salesforce custom fields
  example :
    day : 0;0;2;5;5
    starthours : 100;1200;800;600;1900
    endhours : 300;1400;1800;1200;2200
  In Salesforce fields, data must be stored like this.
